package main

import (
	"context"
	"fmt"
	"testing"
	"time"
)

func TestProducer(t *testing.T) {
	tests := []struct {
		name      string
		limit     int
		expResult []int
	}{
		{
			name:      "limit is 0",
			limit:     0,
			expResult: []int{},
		},
		{
			name:      "limit is 1",
			limit:     1,
			expResult: []int{1},
		},
		{
			name:      "limit is 3",
			limit:     3,
			expResult: []int{1, 2, 3},
		},
	}

	t.Parallel()

	for _, tc := range tests {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			dataCh := producer(tc.limit)
			for _, exp := range tc.expResult {
				var d int

				// timeout to check if producer generates data
				timeout := time.NewTicker(100 * time.Millisecond)
				select {
				case d = <-dataCh:
				case <-timeout.C:
					t.Error("producer did not generate any data")
					return
				}

				if d != exp {
					t.Errorf("expected %d, received %d", exp, d)
					return
				}
			}

			if _, ok := <-dataCh; ok {
				t.Error("producer did not close channel")
				return
			}
		})
	}
}

func TestProcessor(t *testing.T) {
	tests := []struct {
		name      string
		ctx       context.Context
		value     int
		expResult int
		expErr    error
	}{
		{
			name:   "canceled context",
			ctx:    nil, // we will imitate ctx cancellation in test is it is nil
			expErr: context.Canceled,
		},
		{
			name:      "valid result",
			ctx:       context.Background(),
			value:     8,
			expResult: 64,
		},
		{
			name:   "10 always cause error",
			ctx:    context.Background(),
			value:  10,
			expErr: fmt.Errorf("i hate 5"),
		},
	}

	t.Parallel()

	for _, tc := range tests {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			if tc.ctx == nil {
				var cancel func()
				tc.ctx, cancel = context.WithCancel(context.Background())
				cancel()
			}
			r, err := processor(tc.ctx, tc.value)
			if err != nil {
				if tc.expErr.Error() != err.Error() {
					t.Errorf("processor returned unexpected error: %v", err)
				}
				return
			}

			if r != tc.expResult {
				t.Errorf("expected %d, received %d", tc.expResult, r)
			}
		})
	}
}
