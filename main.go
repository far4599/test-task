package main

import (
	"context"
	"errors"
	"fmt"
	"log"
	"sync"
	"time"
)

const (
	limit           = 1000
	concurrencySize = 5
)

// producer generates row of natural numbers from 1 to limit and sends it to the channel
// it creates unbuffered channel to ensure the readers got all values
// before the channel is closed and set to nil
func producer(limit int) chan int {
	ch := make(chan int)
	go func() {
		for i := 1; i <= limit; i++ {
			ch <- i
		}
		close(ch)
		ch = nil // ensure that the readers are not getting default values from the channel any more
	}()
	return ch
}

// processor handles a piece of data and return result
func processor(ctx context.Context, i int) (int, error) {
	if i == 10 {
		return 0, errors.New("i hate 5")
	}
	time.Sleep(5 * time.Second)

	// return error if context is canceled
	select {
	case <-ctx.Done():
		return 0, ctx.Err()
	default:
	}

	return i * i, nil
}

// terminator wants to kill John Connor ;)
func terminator(results chan int) {
	for r := range results {
		fmt.Println(r)
	}
}

// consumer handles data from dataCh with number of concurrent workers and exits on either first error is encountered or all workers are finished
func consumer(ctx context.Context, dataCh chan int, workers int) error {
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	resultCh := make(chan int)
	errCh := make(chan error)

	go terminator(resultCh)

	go func() {
		wg := sync.WaitGroup{}
		for i := 0; i < workers; i++ {
			wg.Add(1)
			go worker(ctx, &wg, dataCh, resultCh, errCh)
		}
		wg.Wait()

		close(resultCh)
		close(errCh)
	}()

	return <-errCh
}

func main() {
	if err := consumer(context.Background(), producer(limit), concurrencySize); err != nil {
		log.Fatal(err)
	}
}

// worker receives data from dataCh, try to process it and write the result to resultCh, exits on:
// - dataCh is closed
// - any error is encountered
// - context is canceled
func worker(ctx context.Context, wg *sync.WaitGroup, dataCh <-chan int, resultCh chan<- int, errCh chan<- error) {
	defer wg.Done()
	for {
		select {
		case <-ctx.Done():
			// handle context cancellation
			select {
			case errCh <- ctx.Err():
			default:
			}
			return
		case d, ok := <-dataCh:
			if !ok {
				fmt.Println("dataCh is closed")
				return
			}
			r, err := processor(ctx, d)
			if err != nil {
				select {
				case errCh <- err:
				default:
				}
				return
			}
			resultCh <- r
		}
	}
}
