default:
	@echo Actions:
	@echo   run

run:
	go run main.go

lint:
	go vet ./...
	golangci-lint run
